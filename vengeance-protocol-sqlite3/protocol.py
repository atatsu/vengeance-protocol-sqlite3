from vengeance.protocol import StorageProtocol, ProtocolSetting


class SQLite3Protocol(StorageProtocol):
	"""
	Vengeance protocol allowing persistent data storage to a sqlite database.
	"""
	database_file = ProtocolSetting(
		default='vengeance.db',
		description='Filename of database (absolute or relative path)'
	)

	async def connect(self, *args, **kwargs):
		pass

	async def grab(self, *args, **kwargs):
		pass

	async def stash(self, *args, **kwargs):
		pass
